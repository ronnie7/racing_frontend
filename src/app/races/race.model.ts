export class Race {
  name: string;
  id: number;
  close_time: any;
  meeting: string;	
  type: string;
  starting_time: any;
}