import { Component, OnDestroy } from "@angular/core";

import { Race } from "./race.model";
import { RaceService } from "./race-service";
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
  selector: "app-list",
  moduleId: module.id,
  templateUrl: "./races-list.component.html",
  styleUrls: ["./races-list.component.css"],
})

export class RacesListComponent {

	races: Race[];

	// private future: Date;
 //    private futureString: string;
 //    private diff: number;
 //    private $counter: Observable<number>;
 //    private subscription: Subscription;
 //    private message: string;

 	text: any = { "Weeks": "Weeks", 
    "Days": "Days", "Hours": "Hours",
     Minutes: "Minutes", "Seconds": "Seconds",
    "MilliSeconds":"MilliSeconds" };

	constructor(private raceService: RaceService ) { 
		
	}

   

	ngOnInit(): void {
		
		this.raceService.list()
	      .then((races) => {
	        this.races = races;	  	      
	    });

	      
	    
	}

}