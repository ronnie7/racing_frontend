import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import "rxjs/add/operator/toPromise";

import { Race } from "./race.model";
import { Competitor } from "./competitor.model";

@Injectable()
export class RaceService {
  constructor(private http: Http) { }

  list() {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");

    return this.http.get("http://racing.dev/api/races/next5", {
      headers: headers
    })
    .toPromise()
    .then((res: Response) => {
      let data = res.json();
      let allRaces = [];

      data.data.forEach((entry) => {
        let race = new Race();
        race.name = entry.name;
        race.id = entry.id;
        race.close_time = entry.close_time;
        race.meeting = entry.meeting.location;
        race.type = entry.meeting.racetype.name;
        race.starting_time = entry.starting_time;
        
        allRaces.push(race);
      });
      console.log(allRaces);
      return allRaces;
    })
    .catch(this.handleError);
  }

  get(id: number) {
    
    let headers = new Headers();
    headers.append("Content-Type", "application/json");

    return this.http.get("http://racing.dev/api/races/" + id, {
      headers: headers
    })
    .toPromise()
    .then((res: Response) => {
      let data = res.json();
      let allcompetitors = [];

      

      data.data.forEach((d) => {

        let competitor = new Competitor();
        
        competitor.name = d.name;
        competitor.id = d.id;
        competitor.position = d.position;
        
        allcompetitors.push(competitor);
      });

    
      return allcompetitors;
    })
    .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || "";
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ""} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Promise.reject(errMsg);
  }
}