import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { Race } from "./race.model";
import { RaceService } from "./race-service";

import { Competitor } from "./competitor.model";

@Component({
  selector: "app-details",
  moduleId: module.id,
  templateUrl: "./race-detail.component.html",
  styleUrls: ["./race-detail.component.css"],
})

export class RaceDetailComponent implements OnInit {
  
  competitors : Competitor[];

  dtOptions: DataTables.Settings = {};

  constructor(private raceService: RaceService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    
    const id = +this.route.snapshot.params["id"];

    this.raceService.get(id)
      .then((results) => { 
        this.competitors = results;         
      });

     
  }
}