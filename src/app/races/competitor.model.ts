export class Competitor {
  name: string;
  id: number;
  position: number;
}