import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { RacesListComponent } from "./races/races-list.component";
import { RaceDetailComponent } from "./races/race-detail.component";

const routes: Routes = [
  { path: "", component: RacesListComponent },
  { path: "details/:id", component: RaceDetailComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }