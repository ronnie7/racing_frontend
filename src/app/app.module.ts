import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { DataTablesModule } from 'angular-datatables';
import { AppComponent } from './app.component';

import { AppRoutingModule } from "./app-routing.module";


import { RacesListComponent } from "./races/races-list.component"
import { RaceDetailComponent } from "./races/race-detail.component"
import { RaceService } from "./races/race-service";
import {CountDown} from "../../node_modules/angular2-simple-countdown/countdown";

@NgModule({
  declarations: [
    AppComponent,
    RacesListComponent,
    RaceDetailComponent,
    CountDown
  ],
  imports: [
    BrowserModule,
    DataTablesModule,
    HttpModule,
    AppRoutingModule,
  ],
  providers: [RaceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
